#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
bloquearPaginas.py v0.1
Creador: Francisco Palos
Gitlab: @palos.francisco
1palos.francisco@gmail.com
"""

from shutil import copy
from pathlib import Path
from time import sleep
from ctypes import windll
from msvcrt import getch
import os

HOSTS = r"C:\Windows\System32\drivers\etc\hosts"
HOSTSBAK = r"C:\Windows\System32\drivers\etc\hosts.bak"
PAGINAS = r"paginas.txt"

def portada():
    print(" _____ _                         _____         _             ")
    print("| __  | |___ ___ _ _ ___ ___ ___|  _  |___ ___|_|___ ___ ___ ")
    print("| __ -| | . | . | | | -_| .'|  _|   __| .'| . | |   | .'|_ -|")
    print("|_____|_|___|_  |___|___|__,|_| |__|  |__,|_  |_|_|_|__,|___|v0.1")
    print("              |_|                         |___|              ")

def veradmin():
    try:
        is_admin = os.getuid() == 0
    except AttributeError:
        is_admin = windll.shell32.IsUserAnAdmin() != 0
    return is_admin

def main():
    portada()
    if Path(HOSTSBAK).is_file() is False:
        copy(HOSTS, HOSTSBAK)
    fPaginas = open(PAGINAS, "r")
    fHosts = open(HOSTS, "a")
    fHosts.write(f"\n# Páginas agregadas por bloquearPaginas. [Inicio]")
    for linea in fPaginas:
        if linea.find("#") == -1:
            linea = linea.replace("\n","")
            fHosts.write(f"\n127.0.0.1 {linea}\n127.0.0.1 www.{linea}")
            print(f"{linea}", end="")
            n = 25 - len(linea)
            j = 0
            while(j < n):
                print(" ", end="")
                j += 1
            print(": ok")
            sleep(0.2)
        else:
            next
    fHosts.write(f"\n# Páginas agregadas por bloquearPaginas. [Final]")
    fPaginas.close()
    fHosts.close()
    sleep(0.5)
    print("Páginas bloqueadas :)")

if __name__ == "__main__":
    if veradmin() == True:
        if Path(PAGINAS).is_file() is True:
            main()
        else:
            print("Archivo de páginas no existe, asegúrate que exista paginas.txt")
    else:
        print("Necesitas ejecutar el programa como Administrador :)")
    print("Presione una tecla para continuar...")
    getch()